import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable, Subscriber } from 'rxjs';
import options from './payment/data';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  public socket: WebSocket;
  readonly uri: string = 'http://localhost:8080';

  public SendData(str) {
    if (this.socket.readyState == WebSocket.OPEN) {
      this.socket.send("str");
    }
  }

  setupSocketConnection() {
    this.socket.onopen = function (event) {
      this.send('{"function" : "ProductList"}')
      console.log("Mahmut xxx2");
    };

    // Listen for messages
    this.socket.addEventListener('message', function (event) {
      console.log('Message from server "' + event.data + '"');
      const msg = JSON.parse(event.data);

      if (msg.hasOwnProperty('Products')) {
        const addProd = (packet, prod) => {
          const price = Number.parseInt((packet === 'starter' ? prod.PRICE_BASIC : (packet === 'standard' ? prod.PRICE_STANDARD : prod.PRICE_ADVANCED)));
          if (Object.keys(options.extraPackets[packet]).includes(prod.PRODUCT_CODE)) {
            if (options.extraPackets[packet][prod.PRODUCT_CODE].hasOwnProperty('id')) {
              const pack = options.extraPackets[packet][prod.PRODUCT_CODE];
              options.extraPackets[packet][prod.PRODUCT_CODE] = {
                [pack.id]: pack,
                [prod.PRODUCT_ID]: {
                  id: prod.PRODUCT_ID,
                  name: prod.PRODUCT_NAME,
                  price
                },
              };
            } else {
              options.extraPackets[packet][prod.PRODUCT_CODE] = {
                ...options.extraPackets[packet][prod.PRODUCT_CODE],
                [prod.PRODUCT_ID]: {
                  id: prod.PRODUCT_ID,
                  name: prod.PRODUCT_NAME,
                  price
                },
              };
            }
          } else {
            options.extraPackets[packet] = {
              ...options.extraPackets[packet],
              [prod.PRODUCT_CODE]: {
                id: prod.PRODUCT_ID,
                name: prod.PRODUCT_NAME,
                price
              },
            }
          }
        }

        msg.Products.forEach(prod => {
          if (prod.PRICE_BASIC !== '') {
            addProd('starter', prod);
          }
          if (prod.PRICE_STANDARD !== '') {
            addProd('standard', prod);
          }
          if (prod.PRICE_ADVANCED !== '') {
            addProd('advanced', prod);
          }
        });
      }
    });
  }

  constructor() {
    this.socket = new WebSocket('ws://localhost:8080');
  }
}
