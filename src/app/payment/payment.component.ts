import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ContentComponent } from '../content/content.component';
import { FormBuilder, FormGroup, FormArray, FormControl, FormsModule, Validators } from '@angular/forms';
import { domainToUnicode } from 'url';
import { element } from 'protractor';
import { format } from 'path';
import { ThrowStmt } from '@angular/compiler';
import { WebSocketService } from '../web-socket.service';
import data from './data';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})

export class PaymentFormComponent implements OnInit {
  keys(obj) {
    return Object.keys(obj);
  }

  constructor(private fb: FormBuilder , private webSocketService: WebSocketService) {

  }

  offerForm: FormGroup;
  activeExtraPackets: Object;
  options = data;

  get selectedPacket() {
    return this.offerForm.get('packet').value;
  }
  get companyAmount() {
    return this.offerForm.get('companyAmount').value;
  }
  get companySelection() {
    return this.offerForm.get('ccSelection').value;
  }
  get depSelection() {
    return this.offerForm.get('depSelection').value;
  }
  get extraPackets() {
    return this.offerForm.get('extraPackets') as FormArray;
  }
  get currentExtras() {
    return this.options['extraPackets'][this.selectedPacket];
  }
  get customer() {
    return this.offerForm.get('customer');
  }
  get customerName() {
    return this.customer.get('name');
  }
  get customerSurname() {
    return this.customer.get('surname');
  }
  get customerPhone() {
    return this.customer.get('phone');
  }
  get customerMail() {
    return this.customer.get('mail');
  }
  get customerOrganization() {
    return this.customer.get('organization');
  }
  get customerCity() {
    return this.customer.get('city');
  }
  get customerTown() {
    return this.customer.get('town');
  }

  private createExtra() {
    const arr = new FormArray([], []) as FormArray;
    Object.keys(this.currentExtras).forEach(p => {
      arr.push(this.fb.group({
        checked: false,
        amount: '1'
      }));
    });
    this.extraPackets.push(arr);
  }

  generateExtras() {
    this.extraPackets.clear();
    this.createExtra();
    if (this.depSelection === 'seperate') {
      for (let i = 0; i < this.companyAmount - 1; i++) {
        this.createExtra();
      }
    }
  }

  extraValidate() {
    let valid = true;
    if (this.extraPackets.length > 0) {
      this.extraPackets.value.forEach(dep => {
        dep.forEach(pack => {
          if (pack.checked !== false && pack.amount < 1) {
            valid = false;
          }
        });
      });
    }

    const exception = this.extraPackets.value[0];
    if (this.selectedPacket === 'standard' && exception && exception[2].amount > 3) valid = false;

    // if extras are valid, pick checked extra packets
    if (valid) {
      this.activeExtraPackets = this.extraPackets.value.map(dep => {
        return dep
          .map((extra, i) => {
            const pack = this.currentExtras[this.keys(this.currentExtras)[i]];
            return {
              name: (extra.checked === true) ? pack.name : (extra.checked === false) ? pack.name : pack[extra.checked].name,
              amount: (extra.checked === false) ? 0 : extra.amount
            }
          })
          .filter(extra => extra.amount > 0);
      });
    }
    return valid;
  }

  onExtraRadioClick(i, j) {
    const packet = this.extraPackets.get([i, j]).value;
    packet.checked = (packet.checked !== false) ? false : packet.checked;
  }

  onAccordionClick(target) {
    target.classList.toggle('active');
    const panel = target.nextElementSibling;
    panel.style.display = (panel.style.display === 'block') ? 'none' : 'block';
  }

  senddata() {
    this.webSocketService.SendData("Mahmut");
  }

  ngOnInit() {
    this.webSocketService.setupSocketConnection();
    this.webSocketService.SendData("Deneme Bea");

    this.offerForm = this.fb.group({
      packet: 'starter',
      ccSelection: 'single',
      companyAmount: '2',
      depSelection: 'all',
      extraPackets: this.fb.array([]),
      customer: this.fb.group({
        name: new FormControl('', [Validators.required]),
        surname: new FormControl('', [Validators.required]),
        phone: new FormControl('', [Validators.required, (control) => {
          if (control.value !== null && control.value.toString().length !== 10) return { valid: false };
          return null;
        }]),
        mail: new FormControl('', [Validators.required, Validators.email]),
        organization: new FormControl('', [Validators.required]),
        city: new FormControl('', [Validators.required]),
        town: new FormControl(''),
      }),

    });

    //jQuery time
    let current_fs, next_fs, previous_fs; //fieldsets
    let left, opacity, scale; //fieldset properties which we will animate
    let animating; //flag to prevent quick multi-click glitches

    // TODO: REFACTOR THIS
    $(document).ready(function () {
      $(".next").click(function () {
        if (animating) return false;
        animating = true;
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({ opacity: 0 }, {
          step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
              'transform': 'scale(' + scale + ')',
              'position': 'absolute'
            });
            next_fs.css({ 'left': left, 'opacity': opacity });
          },
          duration: 800,
          complete: function () {
            current_fs.hide();
            animating = false;
          },
        });
      });

      $(".previous").click(function () {
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({ opacity: 0 }, {
          step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({ 'left': left });
            previous_fs.css({ 'transform': 'scale(' + scale + ')', 'opacity': opacity });
          },
          duration: 800,
          complete: function () {
            current_fs.hide();
            animating = false;
          },
        });
      });

      $(".submit").click(function () {
        return false;
      });
    });
  }
}
