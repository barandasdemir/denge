const options = {
  packets: {
    starter: { name: 'Başlangıç 69TL', price: 69 },
    standard: { name: 'Standart 99TL', price: 99 },
    advanced: { name: 'Gelişmiş 149TL', price: 149 },
  },
  extraPackets: {
    starter: {
    },
    standard: {
    },
    advanced: {
    },
  },
};

export default options;
