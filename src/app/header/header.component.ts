import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }
  ngOnInit(): void {
  }
  
  myFunction() {
    var x = document.getElementById("myTopnav");
    var y = document.getElementById("navigation");
    if (x.className === "topnav") {
      x.className += " responsive";
      y.className += " responsive";
    } else {
      x.className = "topnav";
      y.className = "navigation";
    }
  }
  
   
  
  
}

