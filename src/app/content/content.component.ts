import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  constructor() { }
  ngOnInit(): void {
    this.showSlides(0);

  }
  
  ngAfterViewChecked() {
    this.Btn69Pos();
    this.Btn99Pos();
    this.Btn149Pos();
    this.TryFreeLinkPos();
    this.DevicesImgsPos();

  }

  Btn69Pos() {
    var Img = document.querySelector("#priceImgs");
    var Btn = document.querySelector("#Buy69Btn");
    var ImgWidth: number = Img.clientWidth;
    var ImgHeight: number = Img.clientHeight;
    var BtnWidth: number = ImgWidth * 0.15;
    var BtnPosX: number = ImgWidth * 0.31;
    var BtnPosY: number = ImgHeight * 0.2;
    var BtnHeight: number = ImgHeight * 0.06;
    var BtnFontSize: number = ImgHeight * 0.03;

    Btn.setAttribute("style",
      "left:" + BtnPosX + "px;" +
      "width:" + BtnWidth + "px;" +
      "top:-" + BtnPosY + "px;" +
      "height:" + BtnHeight + "px;" +
      "font-size:" + BtnFontSize + "px;");
  }

  

  Btn99Pos() {
    var Img = document.querySelector("#priceImgs");
    var Btn = document.querySelector("#Buy99Btn");
    var ImgWidth: number = Img.clientWidth;
    var ImgHeight: number = Img.clientHeight;
    var BtnWidth: number = ImgWidth * 0.15;
    var BtnPosX: number = ImgWidth * 0.4;
    var BtnPosY: number = ImgHeight * 0.2;
    var BtnHeight: number = ImgHeight * 0.06;
    var BtnFontSize: number = ImgHeight * 0.03;


    Btn.setAttribute("style",
      "left:" + BtnPosX + "px;" +
      "width:" + BtnWidth + "px;" +
      "top:-" + BtnPosY + "px;" +
      "height:" + BtnHeight + "px;" +
      "font-size:" + BtnFontSize + "px;");
  }

  Btn149Pos() {
    var Img = document.querySelector("#priceImgs");
    var Btn = document.querySelector("#Buy149Btn");
    var ImgWidth: number = Img.clientWidth;
    var ImgHeight: number = Img.clientHeight;
    var BtnWidth: number = ImgWidth * 0.15;
    var BtnPosX: number = ImgWidth * 0.49;
    var BtnPosY: number = ImgHeight * 0.2;
    var BtnHeight: number = ImgHeight * 0.06;
    var BtnFontSize: number = ImgHeight * 0.03;

    Btn.setAttribute("style",
      "left:" + BtnPosX + "px;" +
      "width:" + BtnWidth + "px;" +
      "top:-" + BtnPosY + "px;" +
      "height:" + BtnHeight + "px;" +
      "font-size:" + BtnFontSize + "px;");
  }

  TryFreeLinkPos() {
    var Img = document.querySelector("#priceImgs");
    var Link = document.querySelector("#TryFreeLink");
    var ImgWidth: number = Img.clientWidth;
    var ImgHeight: number = Img.clientHeight;
    var LinkPosX: number = ImgWidth * 0.1;
    var LinkPosY: number = ImgHeight * 0.09;
    var LinkFontSize: number = ImgHeight * 0.03;

    Link.setAttribute("style",
      "left:" + LinkPosX + "px;" +
      "top:-" + LinkPosY + "px;" +
      "font-size:" + LinkFontSize + "px;");

  }

  

  

  showSlides(slideIndex) {

    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
      slides[i].className = "mySlides";
    }

    slideIndex++;
    if (slideIndex > slides.length) { slideIndex = 1 }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].className += " slideblock";
    dots[slideIndex - 1].className += " active";
    setTimeout(() => {
      this.showSlides(slideIndex)
    }, 2000); // Change image every 2 seconds
  }



  DevicesImgsPos() {
    var DevicesImgs = document.querySelector("#DevicesImgs");
    var BlueCard = document.querySelector("#BlueCard");
    var CardWidth: number = BlueCard.clientWidth;
    var CardHeight: number = BlueCard.clientHeight;

    var DevicesImgsPosX: number = CardWidth * 0.52;
    var DevicesImgsPosY: number = CardHeight * 0.45;
    DevicesImgs.setAttribute("style",
      "left:" + DevicesImgsPosX + "px;" +
      "top:-" + DevicesImgsPosY + "px;");
  }


  customOptions: any = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 4
      },
      740: {
        items: 8
      },
      940: {
        items: 7
      },
      1040: {
        items: 9
      },


    },
    nav: false,
    autoplay: true,
    autoplayTimeout: 2000,

  }
}
